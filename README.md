# AStA Newsletter

Dieses Repo beinhaltet nur die Newsletter an sich.

Die gehostete Webseite findest du unter [newsletter.asta.uni-goettingen.de](https://newsletter.asta.uni-goettingen.de).

Die ganzen Sachen zum Bauen der Webseite findest du im [newsletter-webseite Repo](https://gitlab.gwdg.de/asta/dnd-referat/newsletter-webseite).

## How-To: Neuen Newsletter hinzufügen

Um einen neuen Newsletter hinzuzufügen, musst du einfach nur den korrekten Ordner (nach dem Schema `legislature-YYYY_YY/YYYY-MM-DD/`) erstellen (z.B. `legislature-2022_23/2022-04-27/` für den 27. April 2022) und dort den Newsletter als `newsletter.html`-Datei ablegen.

Dann musst du deine Änderung nur noch commiten (und pushen) und schon sollte die Newsletter Webseite innerhalb von einigen Minuten automatisch den neuen Newsletter beinhalten.

## How-To: Neue Newsletter-HTML erstellen

0. Warten bis alle Meldungen inkl. Bilder im Pad eingetragen sind. 
1. Newsletter im [Newsletter Editor](https://asta.pages.gwdg.de/dnd-referat/newslettereditor/) erstellen.
2. Nochmal korrektur lesen (preview) und kopieren (copy).
3. Thunderbird Mail verfassen von `newsletter@asta.uni-goettingen.de` an `asta-news@asta.uni-goettingen.de` mit Betreff 'AStA-Newsletter Monat Jahr'.
4. `Einfügen > HTML...` dort die gesamte HTML reinkopieren und abschicken.
5. Kopierten html-Text in einer Datei namens newsletter.html speichern und hier hochladen.

### Wie füge ich zusätzliche Dateien hinzu?

Manche Newsletter benötigen noch extra Dateien (siehe [Dezember 2020](legislature-2020_21/2020-12-07/),[Januar 2021](legislature-2020_21/2021-01-13/) & [Juni 2021](legislature-2021_22/2021-06-29/)), dann kannst du diese Dateien einfach mit in den Ordner von dem entsprechenden Newsletter ablegen.

Damit diese Dateien dann auch auf der Webseite erscheinen, musst du noch eine `files.json`-Datei in dem Ordner erstellen, welche Inhalt nach dem folgenden Schema haben soll:

```json
{
	"<Dateiname>": {
		"de":"<deutsche Variante des Dokumententitels>",
		"en":"<englische Variante des Dokumententitels>"
	},
	"<weitere Datei>": {
		"de":"<deutsche Variante des Dokumententitels>",
		"en":"<englische Variante des Dokumententitels>"
	}
}
```

### Wie füge ich eine Notiz zu einem Newsletter hinzu?

Manche Newsletter benötigen noch extra Notizen (siehe [Oktober 2021](legislature-2021_22/2021-10-31/)), dann kannst du einfach eine `note.json`-Datei in den Ordner von dem entsprechenden Newsletter ablegen.

Die `note.json`-Datei muss das folgende Schema haben:

```json
{
	"de":"<deutsche Variante der Notiz>",
	"en":"<englische Variante der Notiz>"
}
```

## Deployment

Sobald Inhalt in dieses Repo geladen wird, wird eine CI/CD Pipeline ausgeführt, welche dafür sorgt, dass die Newsletter Webseite aktuallisiert wird.

Das Durchlaufen dieser Pipeline dauert insgesamt ca. 5-10 Minuten.

Heißt wenn du einen neuen Newsletter hochlädst, ist dieser nach ca. 5-10 Minuten automatisch auf der Newsletter Webseite.
